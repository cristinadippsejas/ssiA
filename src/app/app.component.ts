import { Component } from '@angular/core';
import {t} from '@angular/core/src/render3';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  person = {name: 'leo', lastName: 'peres',}

  // para funcionalidad,
  onClick() {
    alert(this.person.name)
    console.log('click');
  }
}
